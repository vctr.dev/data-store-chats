// Adapted from AffinityLive Repo /frontend/lib/utility/data-store/data-store.js
const { AlSubscriptionManager } = require('./subscription-manager.js');

/**
 * @description Manages a single state object. The state can only be updated by dispatching actions,
 * these actions are registered during construction.
 */
class AlDataStore {
	/**
	 * @param {object} registeredActions - Contains action types as keys, the value is a function with
	 * the existing state and payload as params. The action functions should not mutate the original state
	 * instead it should return a new state each time. Example of registeredActions:
	 * {
	 * 		[CHANGED_TITLE]: function(state, payload) {
	 *			return Object.assign({}, state, { title: payload });
	 * 		},
	 * 		[INITIALIZE]: function(state, payload) {
	 *			return Object.assign({}, state, payload);
	 * 		}
	 * 	}
	 */
	constructor({ registeredActions, initialState } = {}) {
		this._registeredActions = registeredActions || {};
		this._subscriptionManager = new AlSubscriptionManager();

		this._subscriptionKey = Symbol('data-store');
		this._state = initialState || {};
	}

	/**
	 * @typedef callback
	 * @param {object} state - The current state of the data store
	 */

	/**
	 * @param {callback} callback
	 */
	subscribe(callback) {
		callback(this._state);

		this._subscriptionManager.subscribe({
			key: this._subscriptionKey,
			callback: () => callback(this._state),
		});
	}

	/**
	 * @typedef action
	 * @property {string} type - The type of action. These should match the registeredActions
	 * @property {any} payload - Data to be used by the action function
	 */

	/**
	 * @param {action} action
	 */
	async dispatch(action = {}) {
		const actionFn = this._registeredActions[action.type];
		this._state = actionFn ? await actionFn(this._state, action.payload) : this._state;
		this._subscriptionManager.publish({ key: this._subscriptionKey });
	}
}

module.exports.AlDataStore = AlDataStore;
