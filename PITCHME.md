# Data Store Chats

- Reactive Programming
- Data Store

---

## What is Reactive Programming?

- When something happens, how do we respond?
	- be told what to do
	- be told that something happened
- Streams of information
- Traffic light analogy
- Guitar analogy
- Decoupling/Dependency Inversion

---

## How did we do stuff?

- Components handle all the data logic
- Problems
	- Data Transparency
	- Change management
	- Hidden coupling
	- Broken single responsibility principle

---

## Introducing the data store

- Service handles all the data logic
- Benefits
	- Single direction data flow
	- Data transparency
	- Clear coupling

---

## Characteristics of data store

- Normalized data
	- No derived data
- Strict interfaces
	- Should not need to know about how the data is stored
	- Should only know the actions available
- Functional interface
	- State in, state out
	- Non-mutating
	- Easy testing

---

## Demonstration

---

## Questions?
